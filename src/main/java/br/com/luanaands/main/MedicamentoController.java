package br.com.luanaands.main;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import br.com.luanaands.modelo.Medicamento;
import br.com.luanaands.repository.MedicamentoRepository;

@RestController
@CrossOrigin("${origem-permitida}")
public class MedicamentoController {

	
	@Autowired
	private MedicamentoRepository medicamentoRepository;
	
	@GetMapping("/itens")
	public List<Medicamento> listar() {
		return (List<Medicamento>) medicamentoRepository.findAll();
	}
	
	@PostMapping("/itens")
	public Medicamento adicionar(@RequestBody @Valid Medicamento item) {
		return medicamentoRepository.save(item);
	}
	
}
