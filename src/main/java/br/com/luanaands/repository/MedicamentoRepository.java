package br.com.luanaands.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.luanaands.modelo.Medicamento;

public interface MedicamentoRepository extends CrudRepository<Medicamento, Long>{

	
	
}
